# RemindMe 2022 (Study Project)
### Java (Maven + Spring) + Docker
It is a web application that allows the user to be reminded about different task he chooses. It reminds the user with email and twitter (with a special account).

This application was hosted on a server configured by a teacher with the config inside ```Server config``` folder.