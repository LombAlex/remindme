# Refresh VPS :

```bash
ssh -l ubuntu vps-b844c848.vps.ovh.net
```

```bash
sudo apt update
sudo apt upgrade
sudo apt install git
sudo apt install docker.io
sudo apt install docker docker-compose

sudo groupadd docker
sudo usermod -aG docker $USER
newgrp docker
docker run hello-world

sudo systemctl enable docker.service
sudo systemctl enable containerd.service
```

# Install

-- generate password-less ssh key

```bash
ssh-keygen -t rsa -C "guillaume.jambet+vps@gmail.com"
```

-- add ssh key to github

```bash
cat /home/ubuntu/.ssh/id_rsa.pub
```

-- clone repository
```bash
git config pull.ff only
git -c core.sshCommand="ssh -i .ssh/id_rsa" clone git@github.com:gjambet/vps-infra.git
cd vps-infra
```

# Configure auto refresh

```bash
crontab -e
0 * * * * cd /home/ubuntu/vps-infra && ./refresh-vps-infra.sh
```

# Generate PAT token on github
1. Generate PAT token on github
2. give package read
3. add to .bashrc 
   github_token= xxx


# Check : 

http://vps-b844c848.vps.ovh.net/chuck-facts


# Configure your DB 

* Beware data are not persisted

## Declare your schema : 
in the directory postgres-init, add a file to create your root user + schema.

## Let your application know the db connection : 
pass the following vars to your container :
- SPRING_DATASOURCE_URL=jdbc:postgresql://postgres:5432/db_***
- SPRING_DATASOURCE_USERNAME=****
- SPRING_DATASOURCE_PASSWORD=****
- SPRING_JPA_HIBERNATE_DDL_AUTO=update

## on VPS : 
Kill local volume

## In pg admin : 
Find the ip of postgres instance : 
docker ps -> get <container-id> of postgresSQL
docker inspect <container-id> | grep IPAddress

## Clean Infra : 
docker system prune --volumes -f
   
   
## Grafana : 
https://grafana.com/docs/loki/latest/getting-started/grafana/
   
Log into your Grafana instance. If this is your first time running Grafana, the username and password are both defaulted to admin.
In Grafana, go to Configuration > Data Sources via the cog icon on the left sidebar.
Click the big + Add data source button.
Choose Loki from the list.
The http URL field should be the address of your Loki server. For example, when running locally or with Docker using port mapping, the address is likely http://localhost:3100. When running with docker-compose or Kubernetes, the address is likely http://loki:3100.
To see the logs, click Explore on the sidebar, select the Loki datasource in the top-left dropdown, and then choose a log stream using the Log labels button.
Learn more about querying by reading about Loki’s query language LogQL.
Read more about Grafana’s Explore feature in the Grafana documentation and on how to search and filter for logs with Loki.

To configure Loki as a datasource via provisioning, see Configuring Grafana via Provisioning. Set the URL in the provisioning.
