#!/usr/bin/env bash

echo $(date -u) "Refresh git repository"
touch git-pull-last-run
git pull

echo $(date -u)  "Login to github"
docker login -u gjambet -p $github_token ghcr.io

echo $(date -u) "Refresh docker images"
docker-compose pull --include-deps

echo $(date -u) "Kill current containers"
docker-compose down --remove-orphans

echo $(date -u) "Refresh docker containers"
docker-compose up -d

echo $(date -u) "Housekeeping"
# docker image prune -f
# docker volume prune -f

echo $(date -u) "Script is over"
