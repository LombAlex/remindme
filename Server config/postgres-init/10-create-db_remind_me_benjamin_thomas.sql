CREATE USER remind_tb_root WITH PASSWORD 'admin!123' CREATEDB;
CREATE DATABASE db_remind_tb
    WITH 
    OWNER = remind_tb_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
