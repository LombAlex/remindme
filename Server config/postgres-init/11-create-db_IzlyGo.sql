CREATE USER izlygo_root WITH PASSWORD 'IzlyGo5!' CREATEDB;
CREATE DATABASE db_izlygo
    WITH 
    OWNER = izlygo_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

