CREATE USER bdgest_root WITH PASSWORD 'bdgest1234' CREATEDB;
CREATE DATABASE db_bdgest
    WITH 
    OWNER = bdgest_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
