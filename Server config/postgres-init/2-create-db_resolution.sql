CREATE USER resolution_root WITH PASSWORD 'lesbgdu74' CREATEDB;
CREATE DATABASE db_resolution
    WITH 
    OWNER = resolution_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
