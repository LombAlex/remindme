CREATE USER personaldashboard_root WITH PASSWORD 'mjzm84z03pa7' CREATEDB;
CREATE DATABASE db_personaldashboard
    WITH 
    OWNER = personaldashboard_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
