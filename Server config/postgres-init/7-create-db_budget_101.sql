CREATE USER budget101_root WITH PASSWORD 'budget101_pass' CREATEDB;
CREATE DATABASE db_budget_101
    WITH 
    OWNER = budget101_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
