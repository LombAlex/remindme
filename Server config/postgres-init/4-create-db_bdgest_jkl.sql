CREATE USER bdgest_jkl_root WITH PASSWORD '7QwPyS96m' CREATEDB;
CREATE DATABASE db_bdgest_jkl
    WITH 
    OWNER = bdgest_jkl_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;