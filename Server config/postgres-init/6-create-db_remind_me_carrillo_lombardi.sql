CREATE USER remind_root WITH PASSWORD 'Remind123!' CREATEDB;
CREATE DATABASE db_remind
    WITH 
    OWNER = remind_root
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    
